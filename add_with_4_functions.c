//Write a program to add two user input numbers using 4 functions.
//activity 4

#include<stdio.h>
float x1(){
    float a;
    printf("Enter number:");
    scanf("%f",&a);
}
float x2()
{
    float b;
    printf("Enter number: ");
    scanf("%f",&b);
}
float result(float y1,float y2){
    return (y1+y2);
}
void display(float res)
{
    printf("Sum is %f",res);
}
int main()
{
    float  y1=x1();
    float  y2=x2();
    float res=result(y1,y2);
    display(res);
    return 0;
}