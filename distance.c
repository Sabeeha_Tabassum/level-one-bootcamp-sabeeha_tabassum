//WAP to find the distance between two point using 4 functions.
//activity 6

#include<stdio.h>
#include<math.h>
int num1()
{
    float a;
    scanf("%f",&a);
    return a;
}
int num2()
{
    float b;
    scanf("%f",&b);
    return b;
}
void print(float res)
{
    printf("Distance=%f ",res);
}
float output(float a1,float b1,float a2,float b2)
{
    return (sqrt(((a2-a1)*(a2-a1))+(b2-b1)*(b2-b1)));
}
int main()
{
    float a1,b1,a2,b2;
    printf("Enter a1: ");
    a1=num1();
    printf("Enter b1: ");
    b1=num2();
    printf("Enter a2: ");
    a2=num1();
    printf("Enter b2: ");
    b2=num2();
    float result=output(a1,b1,a2,b2);
    print(result);
    return 0;
    
    
}