//WAP to find the sum of n fractions.
//activity 10

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct fraction{
  int n;
  int d;
};
typedef struct fraction f;

f parameter();
f sum(f f1, f f2);
int gcd(int p, int q);
int  result(f s);

int main(){
  int n,i;
  f p,z;
  printf("Enter the no. of terms: ");
  scanf("%d",&n);
  f a[n];
  for(i=0;i<n;i++){
    printf("Enter fraction %d\n",i+1);
    a[i] = parameter();
  }
  if(n==1){
    z =a[0];
  }
  else{
    p = a[0];
    for(i=0;i<n-1;i++){
      z = sum(p,a[i+1]);
      p = z;
    }
  }
  result(z);
  return 0;
}

f parameter(){
  f fraction;
  printf("NUMERATOR : ");
  scanf("%d",&fraction.n);
  printf("DENOMINATOR : ");
  scanf("%d",&fraction.d);
  return fraction;
}

int gcd(int p, int q){
  int i,a;
  for(i = 1; i <= p && i <= q; i++)
    {
        if(p % i == 0 && q % i == 0)
            a = i;
    }
    return a;
}

f sum(f f1, f f2){
  f f3;
  int c;
  f3.n = ((f1.n*f2.d)+(f2.n*f1.d));
  f3.d = f1.d*f2.d;
  c = gcd(f3.n,f3.d);
  f3.n = f3.n/c;
  f3.d = f3.d/c;
  return f3;
}

int result(f s){
  printf("The sum of the given fractions : %d / %d\n",s.n,s.d);
  return 0;
}

