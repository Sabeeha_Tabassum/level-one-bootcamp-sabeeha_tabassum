//Write a program to find the sum of n different numbers using 4 functions
//activity 9
#include<stdio.h>

int number(int n)
{
    printf("Enter the number of elements to be added:\n");
    scanf("%d",&n);
    return n;
}

int parameters(int n, int arr[])
{
    int i =0 ;
    for (i =0 ; i<n; i++)
    {
        printf("Enter element %d:\n",i+1);
        scanf("%d",&arr[i]);
    }
}

int solve_sum(int n, int arr[])
{
    int sum=0;
    for (int i=0; i<n; i++)
    {
        sum += arr[i];
    }
    return sum;
}

int result(int n, int arr[])
{
    printf("%d",solve_sum(n,arr));
}

int main()
{
    int n = number(n);
    int arr[n];
    parameters(n, arr);
    solve_sum(n, arr);
    printf("Sum of these %d numbers:\n",n);
    result(n,arr);
    return 0;
}