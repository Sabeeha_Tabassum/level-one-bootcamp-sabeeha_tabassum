//WAP to find the volume of a tromboloid using 4 functions.
//activity 5

#include<stdio.h>
float parameter()
{
    int a;
    scanf("%d",&a);
    return a;
}
void print(float res)
{
    printf("Result=%f",res);
}
float solve(int h,int b,int d)
{
  return((h*d*b)+d);
}
float findvolume(int h,int b,int d)
{
    return(1/3.0*solve(h,b,d)/b*1.0);
}

int main()
{
   int h,d,b;
    float volume;
    printf("Enter h: ");
    h=parameter();
    printf("Enter b: ");
    b=parameter();
    printf("Enter d: ");
    d=parameter();
    volume=findvolume(h,b,d);
    print(volume);
    return 0;
}