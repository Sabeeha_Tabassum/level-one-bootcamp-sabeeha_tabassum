//WAP to find the sum of two fractions.
//activity 8


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct fraction{
  int n;
  int d;
};
typedef struct fraction F;

F input();
F sum(F f1, F f2);
int gcd(int p, int q);
void result(F f1,F f2,F res);

int main(){
  F f1,f2,f3;
  printf("Input the 1st fraction\n");
  f1 = input();
  printf("Input the 2nd fraction\n");
  f2 = input();
  f3 = sum(f1,f2);
  result(f1,f2,f3);
  return 0;
}

F input(){
  F f;
  printf("Numerator : ");
  scanf("%d",&f.n);
  printf("Denominator: ");
  scanf("%d",&f.d);
  return f;
}

int gcd(int p, int q){
  int i,a;
  for(i = 1; i <= p && i <= q; i++)
    {
        if(p % i == 0 && q % i == 0)
            a = i;
    }
    return a;
}

F sum(F f1, F f2){
  F f3;
  int c;
  f3.n = ((f1.n*f2.d)+(f2.n*f1.d));
  f3.d = f1.d*f2.d;
  c = gcd(f3.n,f3.d);
  f3.n = f3.n/c;
  f3.d = f3.d/c;
  return f3;
}

void result(F f1,F f2,F res){
printf("Sum of fraction %d/%d+%d/%d=%d/%d",f1.n,f1.d,f2.n,f2.d,res.n,res.d);
  return 0;
}


